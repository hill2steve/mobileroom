function setToday () {
	var dp = $(".hasDatepicker").data("datepicker");
    var newDt = new Date();

    dp.selectedDay = dp.currentDay = newDt.getDate();
    
    dp.drawMonth = dp.selectedMonth = dp.currentMonth = newDt.getMonth();
    dp.drawYear = dp.selectedYear = dp.currentYear = newDt.getFullYear();
    var dd = dp.selectedDay;
    var mm = dp.drawMonth + 1;

    if(dd<10)
    {
        dd='0'+dd;
    }
    if(mm<10)
    {
        mm='0'+mm;
    }
    
    var output = mm + '/' + dd + '/' + dp.drawYear;
    
    $(".hasDatepicker").datepicker("refresh");
    $("#date").val(output);
    
    
};

function setNewDate(month, day, year) {
	var dp = $(".hasDatepicker").data("datepicker");
	var valMonth = parseInt(month, 10); 
	var valDay = parseInt(day, 10); 
	var valYear = parseInt(year, 10); 
	
    var newDt = new Date(valYear, (valMonth-1), valDay);
    
    dp.selectedDay = dp.currentDay = newDt.getDate();
    
    dp.drawMonth = dp.selectedMonth = dp.currentMonth = newDt.getMonth();
    dp.drawYear = dp.selectedYear = dp.currentYear = newDt.getFullYear();
    var dd = dp.selectedDay;
    var mm = dp.drawMonth + 1;

    if(dd<10)
    {
        dd='0'+dd;
    }
    if(mm<10)
    {
        mm='0'+mm;
    }
    
    var output = mm + '/' + dd + '/' + dp.drawYear;
    
    $(".hasDatepicker").datepicker("refresh");
    $("#date").val(output);
}

function getToday() {
    var yourDate = new Date();    
    var dd = yourDate.getDate();
    var mm = yourDate.getMonth() + 1;

    if(dd<10)
    {
        dd='0'+dd;
    }
    if(mm<10)
    {
        mm='0'+mm;
    }
    
    var output = mm + '/' + dd + '/' + yourDate.getFullYear();
	return output;
};

function checkDateMatch() {
    // get selected date from input box
    var strDate = $('#date').val();
    var dateParts = strDate.split("/");

    // get selected date from calendar view
    var dp = $(".hasDatepicker").data("datepicker");
    var dpx = dp.drawMonth + 1;
    var dpy = dp.selectedDay;
    var dpz = dp.drawYear;

    if(dpy<10)
    {
    	dpy='0'+dpy;
    }
    if(dpx<10)
    {
    	dpx='0'+dpx;
    }
    	
    var date1_month_year = dateParts[0] + '/' + dateParts[2];
    var date2_month_year = dpx + '/' + dpz;


    if (date1_month_year == date2_month_year) {
        setNewDate(dateParts[0], dateParts[1], dateParts[2]);
        return true;
    } else {
        return false;
    }
}

