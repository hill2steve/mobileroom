<?php

namespace OSULibrary\OpenroomBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ReserveController extends Controller
{
	/**
	 * Only needs to know the start time and the room. The rest will be computed.
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function indexAction()
	{
		return $this->render('OSULibraryOpenroomBundle:Default:reserve.html.twig');
	}
}