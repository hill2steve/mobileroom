<?php
 
namespace OSULibrary\OpenroomBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
	public function indexAction()
	{	$em = $this->getDoctrine()->getEntityManager();
		//Gets array of reuslts, twig sorts them out into columns
		$floors = $em->getRepository("OSULibraryOpenroomBundle:Roomgroups");
		$results = $floors->findAll();
		//the fid should be related to the database
		//return $this->render('OSULibraryOpenroomBundle:Default:index.html.twig');
		return $this->render('OSULibraryOpenroomBundle:Default:index.html.twig', array('results'=> $results));
	}
}
 
