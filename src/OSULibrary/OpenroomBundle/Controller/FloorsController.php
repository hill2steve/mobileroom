<?php

namespace OSULibrary\OpenroomBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;

use OSULibrary\OpenroomBundle\Entity\Calendar;

class FloorsController extends Controller
{	
	// room1 ... room n 
	//		=> name
	//		=> int(hour => int(minute => available	
	public $RoomBlocks = Array();

	public function indexAction($fid = 6, $section = 0)
	{	
		//session check
		$request = $this->getRequest();
		if ($request->hasPreviousSession() === TRUE){
			$session = $request->getSession();			
		} else{
			$session = new Session();
			$request->setSession($session);
		}
		//Check for our calendar information.
		if ($request->getMethod() === 'POST') {
		    if ($request->request->get('date', FALSE) ) {
		        // store an attribute for reuse during a later user request
		        $selected_date = $request->request->get('date');
		        $session->set('selectedDate', $selected_date);
		    }
		} else if ($session->has('selectedDate')) {
			$selected_date = $session->get('selectedDate');
		} else {
			//no date defaults to today
			$selected_date = date('m/d/Y');
		}
		//TODO: Refactor the title to include a welcome message
		//setups our floors
		$em = $this->getDoctrine()->getEntityManager();
		$floors = $em->getRepository("OSULibraryOpenroomBundle:Roomgroups");
		$floorsTitle = $floors->findAll();
		$results = $floors->find($fid);
		$name = $results->getRoomgroupname();
		
		
		$rooms = $em->getRepository("OSULibraryOpenroomBundle:Rooms");
		$roomResults = $rooms->findBy(array('roomgroupid' => $fid));
		$splitRoomResults = $this->splitRoomModel($fid);
		//setup our roomblock field;
		$roomsection = $splitRoomResults[$section];
		foreach($roomsection as $room){
			$this->RoomBlocks[$room->getRoomid()]['name'] = $room->getRoomname();
		}
		$this->buildReservationList($selected_date);
		
		$lastSection = $section <= 0 ? sizeof($splitRoomResults) - 1:$section - 1;
		$nextSection = $section >= sizeof($splitRoomResults) - 1 ? 0:$section + 1;
		if ($session->has('user')){
			$username = $session->get('user').getFullname();
		} else{
			$username = "";
		}
		//TODO: change the splitroom variable to only contain the section titles and ids		
		return $this->render('OSULibraryOpenroomBundle:Default:floor.html.twig', array(
				'floors' => $floorsTitle,
				'fid' => $fid, 
				'name' => $name, 
				'section'=>$section, 
				'lastSection'=>$lastSection, 
				'nextSection'=>$nextSection, 
				'splitRooms'=>$splitRoomResults,
				'selected_date' => $selected_date,
				'room_blocks' => $this->RoomBlocks,
				'username' => $username)
				);
	}
	/**
	 * Builds a list of "reserved" rooms and "available" rooms depending on the date. Should build these reservations for each 10 minute interval
	 * throughout the day.
	 * 
	 * Takes the date, builds a query for that date and the rooms the user is looking at, then grabs all the reservations for those parameters and 
	 * builds our list. 
	 * 
	 * 
	 * @param $date	today's date in m/d/Y format. Conversions are internal
	 * @param $rooms	The list of roomids which need searched for reservations
	 * @return	Returns a list of timeslots. $list['room']['timehour'] returns taken or available.	
 	 */
	
	//TODO: Make this function respect the cushion. This cannot be done now but must be done when the 
	//reservation controller is up
	private function buildReservationList($date)
	{
		$em = $this->getDoctrine()->getEntityManager();
		$date = strtotime($date);

		//convert to unix time, make sure that we are on an even date calculate end time, then put in mysql format
		$starttime = date("Y-m-d H:i:s", $date);
		$endtime = date("Y-m-d H:i:s" ,$date + 23*60*60 + 59 * 60 + 59);
		
		//testing the query to make sure it gets something. nothing is in the database right now. 
		//$fd = fopen('/home/apache/log','w');
		//should be of size four or less
		
		foreach ($this->RoomBlocks as $key => $block){
			//foreach timeslot, check it against each reservation to make sure it's available, if it's not, mark it as unavailable
			$querybuilder = $em->createQueryBuilder();
			$querybuilder->select("r")
			->from("OSULibraryOpenroomBundle:Reservations", "r")
			->where("(r.roomid = ".$key." )
			  		   		  AND r.end >= '".$starttime."'
			  		   		  AND r.start <= '".$endtime."'"
			);
			$query = $querybuilder->getQuery();
			//start=>date, end=>date, username
			//$fd = fopen('/home/apache/logs','w');
			$qresult = $query->getResult();
			if(sizeof($qresult) == 0){
				//initialize if empty
				for($h = 0; $h < 24; $h++){
					for($m = 0; $m < 60; $m += 10){
						$this->RoomBlocks[$key][$h][$m] = TRUE;
					}
				}
			}
			
			foreach($qresult as $reservation){
				//choose time later
				for($h = 0; $h < 24; $h++){
					for($m = 0; $m < 60; $m += 10){
						if($reservation->isAvailable($date + $h * 60 * 60 + $m * 60)){
							$this->RoomBlocks[$key][$h][$m] = TRUE;
						} else{
							//fputs($fd, date('Y-m-d H:i:s', $date + $h * 60 * 60 + $m * 60) . "\n");
							$this->RoomBlocks[$key][$h][$m] = 0;
						}
					}
				}
			}
		}
	}
	
	
	/**
	 * Function that find and groups rooms by 4 for a floor such that there is no remainder.
	 * 
	 * Creates a 2D array that will hold the rooms of a floor in groups of 4 rooms. $n is the number of full groups of 4 and $r is the remainder of rooms left.
	 * The outer loop is the array that contains the groups of 4 rooms while the inner loop is the array that contains the rooms of each group.
	 * The rooms from roomResults will be placed in roomArr until the remainder is left in which it will group the remainder plus 4 minus the remainder to get a group of 4. 
	 * @param $fid
	 * @return multitype:multitype:
	 */
	public function splitRoomModel($fid)
	{
		$em = $this->getDoctrine()->getEntityManager();
		$rooms = $em->getRepository("OSULibraryOpenroomBundle:Rooms");
		$roomResults = $rooms->findBy(array('roomgroupid' => $fid), array('roomname' => 'ASC'));
		$roomArr = array(array());
		$n = floor(sizeof($roomResults)/4);
		$r = sizeof($roomResults)%4;
		if($n == 0){
			for($i=0; $i<$r; $i++){
				$roomArr[0][$i] = $roomResults[$i];
			}
		}
		else{
			for($i=0; $i<=$n; $i++){
				for($j=0; $j<4; $j++){
					if($i == $n){
						$roomArr[$i][$j] = $roomResults[$i*4+$j-(4-$r)];
						continue;
					}
					$roomArr[$i][$j] = $roomResults[$i*4+$j];
				}
			}
		}
		return $roomArr;
	}
	
}
