<?php

namespace OSULibrary\OpenroomBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class HomeController extends Controller
{
	public function indexAction()
	{
		return $this->render('OSULibraryOpenroomBundle:Default:home.html.twig');;
	}
}