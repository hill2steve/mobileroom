<?php
namespace OSULibrary\OpenroomBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class TestController extends Controller
{	//The thing in the route tells symfony which action to use (in this case index)
	public function indexAction()
	{
		$em = $this->getDoctrine()->getEntityManager();
		$name = '';
		//Gets array of reuslts, twig sorts them out into columsn
		$floors = $em->getRepository("OSULibraryOpenroomBundle:Roomgroups");
		$results = $floors->findAll();
		$dyn['test'] = 'brbrbrbrbrhuehuehue';
	
		
		//The format goes bundle:Entity
		//it IS case sensitive and there are other similar functions you can use.
		//$results = $em->find("OSULibraryOpenroomBundle:Roomgroups", "11");
		
		//$name = $results->getRoomgroupname();
		//I think it needs an array so that it knows how to look up the result
		return $this->render('OSULibraryOpenroomBundle:Default:TestView.html.twig', array('name' => $name, 'results' => $results, 'dyn'=> $dyn));
		//return $this->render('OSULibraryOpenroomBundle:Default:TestView.html.twig', array('results' =>$results));
		
	}
}