<?php

namespace OSULibrary\OpenroomBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Our dummy controller for authenticating. Eventually all of this information needs to get stuffed into a token that goes somewhere magical (idk where)
 */
class DummyAuthController extends Controller
{
		
	public function indexAction()
	{
		$request = $this->getRequest();
		$referer = $request->headers->get('referer');       
		
		if ($request->hasPreviousSession() === TRUE){
			// TODO: Figure out what login has to do with this; probably makes this button activate logout instead
			//redirect->logout?
			$session = $this->getRequest()->getSession();
			$session->set('last', $referer);
		} else {
			$session = new Session();
			$session->set('last', $referer);
		}
		
		$url = "/dumblogin_check";
		$location = $this->getRequest()->getBaseUrl();
		//$route = $this->container->get('_route');

		return $this->redirect($location . $url);
	}
	
	/**
	 * For forcing our authentication method we can have login_check send the user here. 
	 * @param previous - Where the user was previously looking.
	 * @throws AuthenticationException
	 */
	public function CasAuthenticationAction($previous){
		$cas_host = 'login.oregonstate.edu';
		$cas_context = '/cas';
		$cas_port = 443;
		$result = phpCAS::isAuthenticated();
		if($result === true){
			$authenticatedToken = new CasUserToken(true);
			$attributes = phpCAS::getAttributes();
				
			/*
			 *  [osuuid] => 73568895301 [UDC_IDENTIFIER] => A91B06FA65BD211058A70A894179ACA9
			*  [uid] => hillst [email] => hillst@onid.orst.edu
			*  [osupidm] => 1975957 [lastname] => Hill
			*  [firstname] => Steven
			*  [fullname] => Hill, Steven T )
			*/
			//It's important to note tha we don't do all of this the symfony way.
			//we essentially skip the provider to go straight to the user bc CAS handles all the auth for us.
			$user = new OpenroomUser($attributes);
			//... new idea
			$authenticatedToken->setUser($user);
				
		} else {
			throw new AuthenticationException('The CAS authentication failed. Likely because of an invalid Cookie');
		}
		//we want this to do something..
		//$this->container->route()
		return $this->redirect($previous);
	}
	
	/**
	 * A dummy authenticator for testing with while we don't have CAS, uses my info.
	 */
	public function DummyAuthAction(){
		if ($this->getRequest()->hasPreviousSession() === TRUE){
			// TODO: Figure out what login has to do with this; probably makes this button activate logout instead
			//redirect->logout?
			$url = $this->getRequest()->getSession()->get('last');
			
		}
		if ($url == ''){
			$url = $this->getRequest()->getBaseUrl() . "/home";
		}
		//artifact from trying to use symfony authentication
		//$authenticatedToken = new CasUserToken(true);
		$attributes = array(
			"osuuid" => 73568895301, 
			"UDC_IDENTIFIER" => "A91B06FA65BD211058A70A894179ACA9",
			"uid" => "hillst", 
			"email" => "hillst@onid.orst.edu",
			"osupidm" => 1975957, 
			"lastname" => "Hill",
			"firstname" => "Steven",
			"fullname" => "Hill, Steven T"  
			);
		$this->getRequest()->getSession()->set('fullname', $attributes['fullname']);
		$this->getRequest()->getSession()->set('uid', $attributes['uid']);
		$this->getRequest()->getSession()->set('sidhash', '12345');
		$this->getRequest()->getSession()->set('email', $attributes['email']);
		
		//Well I deleted this so uh...
		//$user = new OpenroomUser($attributes);
		return $this->redirect($url);
	}
}