<?php 
namespace OSULibrary\OpenroomBundle\Security\User;

use Symfony\Component\Security\Core\User\UserInterface;

class OpenroomUser
{
    private $email;
    private $fullname;
    private $status;
    private $minutes_limit;

    public function __construct(array $attributes)
    {
        $this->email = $attributes['email'];
        $this->fullname = $attributes['fullname'];
        $this->username = $attributes['uid'];
        $this->status = $attributes['status'];
        $this->setReservationLimit($this->status);
        //lookup roles
    }
    public function getEmail()
    {
        return $this->email;
    }

    public function getFullname()
    {
        return $this->fullname;
    }
    public function getReservationLimit()
    {
    	return $this->minutes_limit;
    }
    
    public function isAuthentic(){
    	return true;
    }
    /**
     * Use this function to force an update of the reservationlimit. Probably should be done before
     * Every reservation is done.
     */
    public function setReservationLimit($status)
    {
		//can't get database from here so do it anytime you try to reach this.
    	//TODO: this is implementation based, should come from database, marking it for future purposes
    	$levels  = array (
	    	"GRADUATE" => 360,
	    	"FACULTY" => 360,
	    	"PROFESSIONAL" => 360,
	    	"DEFAULT" => 180,
    		"UNDERGRADUATE" => 180
	    	);
    	//TODO check contains
    	$mins =  $levels[strtoupper($status)];
    	//check if it's empty
    	$mins === "" ? 180 : $mins;
    	$this->minutes_limit = $mins;
    }
    
    public function equals(UserInterface $user)
    {
    	if (!$user instanceof WebserviceUser) {
    		return false;
    	}
    
    	if ($this->username !== $user->getUsername) {
    		return false;
    	}
    
    	return true;
    }


}