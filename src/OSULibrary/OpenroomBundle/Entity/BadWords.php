<?php

namespace OSULibrary\OpenroomBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BadWords
 *
 * @ORM\Table(name="bad_words")
 * @ORM\Entity
 */
class BadWords
{
    /**
     * @var string
     *
     * @ORM\Column(name="word", type="string", length=40, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $word;

    /**
     * @var string
     *
     * @ORM\Column(name="replacement", type="string", length=40, nullable=false)
     */
    private $replacement;



    /**
     * Get word
     *
     * @return string 
     */
    public function getWord()
    {
        return $this->word;
    }

    /**
     * Set replacement
     *
     * @param string $replacement
     * @return BadWords
     */
    public function setReplacement($replacement)
    {
        $this->replacement = $replacement;
    
        return $this;
    }

    /**
     * Get replacement
     *
     * @return string 
     */
    public function getReplacement()
    {
        return $this->replacement;
    }
}