<?php

namespace OSULibrary\OpenroomBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Bannerlookup
 *
 * @ORM\Table(name="BannerLookup")
 * @ORM\Entity
 */
class Bannerlookup
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="onid", type="string", length=9, nullable=false)
     */
    private $onid;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=30, nullable=false)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=128, nullable=false)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="fullName", type="string", length=41, nullable=false)
     */
    private $fullname;

    /**
     * @var string
     *
     * @ORM\Column(name="idHash", type="string", length=128, nullable=false)
     */
    private $idhash;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set onid
     *
     * @param string $onid
     * @return Bannerlookup
     */
    public function setOnid($onid)
    {
        $this->onid = $onid;
    
        return $this;
    }

    /**
     * Get onid
     *
     * @return string 
     */
    public function getOnid()
    {
        return $this->onid;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Bannerlookup
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Bannerlookup
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set fullname
     *
     * @param string $fullname
     * @return Bannerlookup
     */
    public function setFullname($fullname)
    {
        $this->fullname = $fullname;
    
        return $this;
    }

    /**
     * Get fullname
     *
     * @return string 
     */
    public function getFullname()
    {
        return $this->fullname;
    }

    /**
     * Set idhash
     *
     * @param string $idhash
     * @return Bannerlookup
     */
    public function setIdhash($idhash)
    {
        $this->idhash = $idhash;
    
        return $this;
    }

    /**
     * Get idhash
     *
     * @return string 
     */
    public function getIdhash()
    {
        return $this->idhash;
    }
}