<?php

namespace OSULibrary\OpenroomBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reservationoptions
 *
 * @ORM\Table(name="reservationoptions")
 * @ORM\Entity
 */
class Reservationoptions
{
    /**
     * @var string
     *
     * @ORM\Column(name="optionname", type="string", length=255, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $optionname;

    /**
     * @var string
     *
     * @ORM\Column(name="optionvalue", type="string", length=700, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $optionvalue;

    /**
     * @var \Reservations
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Reservations")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="reservationid", referencedColumnName="reservationid")
     * })
     */
    private $reservationid;



    /**
     * Set optionname
     *
     * @param string $optionname
     * @return Reservationoptions
     */
    public function setOptionname($optionname)
    {
        $this->optionname = $optionname;
    
        return $this;
    }

    /**
     * Get optionname
     *
     * @return string 
     */
    public function getOptionname()
    {
        return $this->optionname;
    }

    /**
     * Set optionvalue
     *
     * @param string $optionvalue
     * @return Reservationoptions
     */
    public function setOptionvalue($optionvalue)
    {
        $this->optionvalue = $optionvalue;
    
        return $this;
    }

    /**
     * Get optionvalue
     *
     * @return string 
     */
    public function getOptionvalue()
    {
        return $this->optionvalue;
    }

    /**
     * Set reservationid
     *
     * @param \OSULibrary\OpenroomBundle\Entity\Reservations $reservationid
     * @return Reservationoptions
     */
    public function setReservationid(\OSULibrary\OpenroomBundle\Entity\Reservations $reservationid)
    {
        $this->reservationid = $reservationid;
    
        return $this;
    }

    /**
     * Get reservationid
     *
     * @return \OSULibrary\OpenroomBundle\Entity\Reservations 
     */
    public function getReservationid()
    {
        return $this->reservationid;
    }
}