<?php

namespace OSULibrary\OpenroomBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Keycards
 *
 * @ORM\Table(name="keycards")
 * @ORM\Entity
 */
class Keycards
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="barcode", type="bigint", nullable=false)
     */
    private $barcode;

    /**
     * @var \Rooms
     *
     * @ORM\ManyToOne(targetEntity="Rooms")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="roomid", referencedColumnName="roomid")
     * })
     */
    private $roomid;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set barcode
     *
     * @param integer $barcode
     * @return Keycards
     */
    public function setBarcode($barcode)
    {
        $this->barcode = $barcode;
    
        return $this;
    }

    /**
     * Get barcode
     *
     * @return integer 
     */
    public function getBarcode()
    {
        return $this->barcode;
    }

    /**
     * Set roomid
     *
     * @param \OSULibrary\OpenroomBundle\Entity\Rooms $roomid
     * @return Keycards
     */
    public function setRoomid(\OSULibrary\OpenroomBundle\Entity\Rooms $roomid = null)
    {
        $this->roomid = $roomid;
    
        return $this;
    }

    /**
     * Get roomid
     *
     * @return \OSULibrary\OpenroomBundle\Entity\Rooms 
     */
    public function getRoomid()
    {
        return $this->roomid;
    }
}