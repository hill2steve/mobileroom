<?php

namespace OSULibrary\OpenroomBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reservations
 *
 * @ORM\Table(name="reservations")
 * @ORM\Entity
 */
class Reservations
{
	/**
	 * Availabiliy[HH:MM] = boolean
	 * @var Array
	 */
	private $availability;
    /**
     * @var integer
     *
     * @ORM\Column(name="reservationid", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $reservationid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start", type="datetime", nullable=false)
     */
    private $start;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end", type="datetime", nullable=false)
     */
    private $end;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=256, nullable=false)
     */
    private $username;

    /**
     * @var integer
     *
     * @ORM\Column(name="numberingroup", type="integer", nullable=false)
     */
    private $numberingroup;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timeofrequest", type="datetime", nullable=false)
     */
    private $timeofrequest;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=60, nullable=true)
     */
    private $email;

    /**
     * @var integer
     *
     * @ORM\Column(name="cleaning", type="smallint", nullable=false)
     */
    private $cleaning;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var \Rooms
     *
     * @ORM\ManyToOne(targetEntity="Rooms")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="roomid", referencedColumnName="roomid")
     * })
     */
    private $roomid;

    /**
     * @var \Keycards
     *
     * @ORM\ManyToOne(targetEntity="Keycards")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="keycardid", referencedColumnName="barcode")
     * })
     */
    private $keycardid;
    /**
     * Checks the time passed against the reservation, if it is available here, then it returns true. 
     * Otherwise it returns false.
     * 
     * @param unixtime $time
     * @return boolean
     */
    public function isAvailable($time){
    	$start = $this->start;
    	$end =  $this->end;
    	//if starts before or at and ends after; if $time is between start and end
    	if($this->start->getTimestamp() <= $time && $this->end->getTimestamp() > $time ){
    		return FALSE;
    	}
    	else return TRUE;
    }
	
    /**
     * Get reservationid
     *
     * @return integer 
     */
    public function getReservationid()
    {
        return $this->reservationid;
    }

    /**
     * Set start
     *
     * @param \DateTime $start
     * @return Reservations
     */
    public function setStart($start)
    {
        $this->start = $start;
    
        return $this;
    }

    /**
     * Get start
     *
     * @return \DateTime 
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set end
     *
     * @param \DateTime $end
     * @return Reservations
     */
    public function setEnd($end)
    {
        $this->end = $end;
    
        return $this;
    }

    /**
     * Get end
     *
     * @return \DateTime 
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return Reservations
     */
    public function setUsername($username)
    {
        $this->username = $username;
    
        return $this;
    }

    /**
     * Get username
     *
     * @return string 
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set numberingroup
     *
     * @param integer $numberingroup
     * @return Reservations
     */
    public function setNumberingroup($numberingroup)
    {
        $this->numberingroup = $numberingroup;
    
        return $this;
    }

    /**
     * Get numberingroup
     *
     * @return integer 
     */
    public function getNumberingroup()
    {
        return $this->numberingroup;
    }

    /**
     * Set timeofrequest
     *
     * @param \DateTime $timeofrequest
     * @return Reservations
     */
    public function setTimeofrequest($timeofrequest)
    {
        $this->timeofrequest = $timeofrequest;
    
        return $this;
    }

    /**
     * Get timeofrequest
     *
     * @return \DateTime 
     */
    public function getTimeofrequest()
    {
        return $this->timeofrequest;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Reservations
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set cleaning
     *
     * @param integer $cleaning
     * @return Reservations
     */
    public function setCleaning($cleaning)
    {
        $this->cleaning = $cleaning;
    
        return $this;
    }

    /**
     * Get cleaning
     *
     * @return integer 
     */
    public function getCleaning()
    {
        return $this->cleaning;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Reservations
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set roomid
     *
     * @param \OSULibrary\OpenroomBundle\Entity\Rooms $roomid
     * @return Reservations
     */
    public function setRoomid(\OSULibrary\OpenroomBundle\Entity\Rooms $roomid = null)
    {
        $this->roomid = $roomid;
    
        return $this;
    }

    /**
     * Get roomid
     *
     * @return \OSULibrary\OpenroomBundle\Entity\Rooms 
     */
    public function getRoomid()
    {
        return $this->roomid;
    }

    /**
     * Set keycardid
     *
     * @param \OSULibrary\OpenroomBundle\Entity\Keycards $keycardid
     * @return Reservations
     */
    public function setKeycardid(\OSULibrary\OpenroomBundle\Entity\Keycards $keycardid = null)
    {
        $this->keycardid = $keycardid;
    
        return $this;
    }

    /**
     * Get keycardid
     *
     * @return \OSULibrary\OpenroomBundle\Entity\Keycards 
     */
    public function getKeycardid()
    {
        return $this->keycardid;
    }
}