<?php

namespace OSULibrary\OpenroomBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Roomspecialhours
 *
 * @ORM\Table(name="roomspecialhours")
 * @ORM\Entity
 */
class Roomspecialhours
{
    /**
     * @var integer
     *
     * @ORM\Column(name="roomspecialhoursid", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $roomspecialhoursid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fromrange", type="datetime", nullable=false)
     */
    private $fromrange;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="torange", type="datetime", nullable=false)
     */
    private $torange;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start", type="time", nullable=false)
     */
    private $start;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end", type="time", nullable=false)
     */
    private $end;

    /**
     * @var \Rooms
     *
     * @ORM\ManyToOne(targetEntity="Rooms")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="roomid", referencedColumnName="roomid")
     * })
     */
    private $roomid;



    /**
     * Get roomspecialhoursid
     *
     * @return integer 
     */
    public function getRoomspecialhoursid()
    {
        return $this->roomspecialhoursid;
    }

    /**
     * Set fromrange
     *
     * @param \DateTime $fromrange
     * @return Roomspecialhours
     */
    public function setFromrange($fromrange)
    {
        $this->fromrange = $fromrange;
    
        return $this;
    }

    /**
     * Get fromrange
     *
     * @return \DateTime 
     */
    public function getFromrange()
    {
        return $this->fromrange;
    }

    /**
     * Set torange
     *
     * @param \DateTime $torange
     * @return Roomspecialhours
     */
    public function setTorange($torange)
    {
        $this->torange = $torange;
    
        return $this;
    }

    /**
     * Get torange
     *
     * @return \DateTime 
     */
    public function getTorange()
    {
        return $this->torange;
    }

    /**
     * Set start
     *
     * @param \DateTime $start
     * @return Roomspecialhours
     */
    public function setStart($start)
    {
        $this->start = $start;
    
        return $this;
    }

    /**
     * Get start
     *
     * @return \DateTime 
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set end
     *
     * @param \DateTime $end
     * @return Roomspecialhours
     */
    public function setEnd($end)
    {
        $this->end = $end;
    
        return $this;
    }

    /**
     * Get end
     *
     * @return \DateTime 
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * Set roomid
     *
     * @param \OSULibrary\OpenroomBundle\Entity\Rooms $roomid
     * @return Roomspecialhours
     */
    public function setRoomid(\OSULibrary\OpenroomBundle\Entity\Rooms $roomid = null)
    {
        $this->roomid = $roomid;
    
        return $this;
    }

    /**
     * Get roomid
     *
     * @return \OSULibrary\OpenroomBundle\Entity\Rooms 
     */
    public function getRoomid()
    {
        return $this->roomid;
    }
}