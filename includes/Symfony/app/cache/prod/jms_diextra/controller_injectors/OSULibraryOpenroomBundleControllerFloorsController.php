<?php

namespace OSULibrary\OpenroomBundle\Controller;

/**
 * This code has been auto-generated by the JMSDiExtraBundle.
 *
 * Manual changes to it will be lost.
 */
class FloorsController__JMSInjector
{
    public static function inject($container) {
        $instance = new \OSULibrary\OpenroomBundle\Controller\FloorsController();
        return $instance;
    }
}
