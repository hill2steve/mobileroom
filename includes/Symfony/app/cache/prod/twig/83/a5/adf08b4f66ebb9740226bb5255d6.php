<?php

/* OSULibraryOpenroomBundle:Default:hour-grid.html.twig */
class __TwigTemplate_83a5adf08b4f66ebb9740226bb5255d6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div data-role=\"collapsible\" data-collapsed-icon=\"arrow-r\" data-expanded-icon=\"arrow-d\">
<h3>";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["grid_hour"]) ? $context["grid_hour"] : null), "html", null, true);
        echo ":00 ";
        echo twig_escape_filter($this->env, (isset($context["grid_period"]) ? $context["grid_period"] : null), "html", null, true);
        echo "</h3>
\t\t\t\t
\t<div class=\"ui-grid-d\" data-type=\"horizontal\">
\t\t<div class=\"ui-block-a\"><h5>Rooms:</h5></div>
\t\t<div class=\"ui-block-b\"><h5>";
        // line 6
        echo twig_escape_filter($this->env, (isset($context["grid_room_a"]) ? $context["grid_room_a"] : null), "html", null, true);
        echo "</h5></div>
\t\t<div class=\"ui-block-c\"><h5>";
        // line 7
        echo twig_escape_filter($this->env, (isset($context["grid_room_b"]) ? $context["grid_room_b"] : null), "html", null, true);
        echo "</h5></div>
\t\t<div class=\"ui-block-d\"><h5>";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["grid_room_c"]) ? $context["grid_room_c"] : null), "html", null, true);
        echo "</h5></div>
\t\t<div class=\"ui-block-e\"><h5>";
        // line 9
        echo twig_escape_filter($this->env, (isset($context["grid_room_d"]) ? $context["grid_room_d"] : null), "html", null, true);
        echo "</h5></div>
\t</div>
\t<div class=\"ui-grid-d\">
\t\t<div class=\"ui-block-a\"><h5>";
        // line 12
        echo twig_escape_filter($this->env, (isset($context["grid_hour"]) ? $context["grid_hour"] : null), "html", null, true);
        echo ":00 ";
        echo twig_escape_filter($this->env, (isset($context["grid_period"]) ? $context["grid_period"] : null), "html", null, true);
        echo "</h5></div>
\t\t<div class=\"ui-block-b\"><a data-role=\"button\" href=\"#page1\" data-icon=\"check\" data-iconpos=\"notext\"></a></div>
\t\t<div class=\"ui-block-c\"><a data-role=\"button\" href=\"#page1\" data-icon=\"check\" data-iconpos=\"notext\"></a></div>
\t\t<div class=\"ui-block-d\"><a data-role=\"button\" href=\"#page1\" data-icon=\"check\" data-iconpos=\"notext\"></a></div>
\t\t<div class=\"ui-block-e\"><a data-role=\"button\" href=\"#page1\" data-icon=\"check\" data-iconpos=\"notext\"></a></div>\t
\t\t
\t</div>
\t<div class=\"ui-grid-d\">
\t\t<div class=\"ui-block-a\"><h5>";
        // line 20
        echo twig_escape_filter($this->env, (isset($context["grid_hour"]) ? $context["grid_hour"] : null), "html", null, true);
        echo ":10 ";
        echo twig_escape_filter($this->env, (isset($context["grid_period"]) ? $context["grid_period"] : null), "html", null, true);
        echo "</h5></div>
\t\t<div class=\"ui-block-b\"><a data-role=\"button\" href=\"#page1\" data-icon=\"check\" data-iconpos=\"notext\"></a></div>
\t\t<div class=\"ui-block-c\"><a data-role=\"button\" href=\"#page1\" data-icon=\"check\" data-iconpos=\"notext\"></a></div>
\t\t<div class=\"ui-block-d\"><a data-role=\"button\" href=\"#page1\" data-icon=\"check\" data-iconpos=\"notext\"></a></div>
\t\t<div class=\"ui-block-e\"><a data-role=\"button\" href=\"#page1\" data-icon=\"check\" data-iconpos=\"notext\"></a></div>\t
\t\t
\t</div>
\t<div class=\"ui-grid-d\">
\t\t<div class=\"ui-block-a\"><h5>";
        // line 28
        echo twig_escape_filter($this->env, (isset($context["grid_hour"]) ? $context["grid_hour"] : null), "html", null, true);
        echo ":20 ";
        echo twig_escape_filter($this->env, (isset($context["grid_period"]) ? $context["grid_period"] : null), "html", null, true);
        echo "</h5></div>
\t\t<div class=\"ui-block-b\"><a data-role=\"button\" href=\"#page1\" data-icon=\"check\" data-iconpos=\"notext\"></a></div>
\t\t<div class=\"ui-block-c\"><a data-role=\"button\" href=\"#page1\" data-icon=\"check\" data-iconpos=\"notext\"></a></div>
\t\t<div class=\"ui-block-d\"><a data-role=\"button\" href=\"#page1\" data-icon=\"check\" data-iconpos=\"notext\"></a></div>
\t\t<div class=\"ui-block-e\"><a data-role=\"button\" href=\"#page1\" data-icon=\"check\" data-iconpos=\"notext\"></a></div>
\t</div>
\t<div class=\"ui-grid-d\">
\t\t<div class=\"ui-block-a\"><h5>";
        // line 35
        echo twig_escape_filter($this->env, (isset($context["grid_hour"]) ? $context["grid_hour"] : null), "html", null, true);
        echo ":30 ";
        echo twig_escape_filter($this->env, (isset($context["grid_period"]) ? $context["grid_period"] : null), "html", null, true);
        echo "</h5></div>
\t\t<div class=\"ui-block-b\"><a data-role=\"button\" href=\"#page1\" data-icon=\"check\" data-iconpos=\"notext\"></a></div>
\t\t<div class=\"ui-block-c\"><a data-role=\"button\" href=\"#page1\" data-icon=\"check\" data-iconpos=\"notext\"></a></div>
\t\t<div class=\"ui-block-d\"><a data-role=\"button\" href=\"#page1\" data-icon=\"check\" data-iconpos=\"notext\"></a></div>
\t\t<div class=\"ui-block-e\"><a data-role=\"button\" href=\"#page1\" data-icon=\"check\" data-iconpos=\"notext\"></a></div>
\t</div>
\t<div class=\"ui-grid-d\">
\t\t<div class=\"ui-block-a\"><h5>";
        // line 42
        echo twig_escape_filter($this->env, (isset($context["grid_hour"]) ? $context["grid_hour"] : null), "html", null, true);
        echo ":40 ";
        echo twig_escape_filter($this->env, (isset($context["grid_period"]) ? $context["grid_period"] : null), "html", null, true);
        echo "</h5></div>
\t\t<div class=\"ui-block-b\"><a data-role=\"button\" href=\"#page1\" data-icon=\"check\" data-iconpos=\"notext\"></a></div>
\t\t<div class=\"ui-block-c\"><a data-role=\"button\" href=\"#page1\" data-icon=\"check\" data-iconpos=\"notext\"></a></div>
\t\t<div class=\"ui-block-d\"><a data-role=\"button\" href=\"#page1\" data-icon=\"check\" data-iconpos=\"notext\"></a></div>
\t\t<div class=\"ui-block-e\"><a data-role=\"button\" href=\"#page1\" data-icon=\"check\" data-iconpos=\"notext\"></a></div>
\t</div>
\t<div class=\"ui-grid-d\">
\t\t<div class=\"ui-block-a\"><h5>";
        // line 49
        echo twig_escape_filter($this->env, (isset($context["grid_hour"]) ? $context["grid_hour"] : null), "html", null, true);
        echo ":50 ";
        echo twig_escape_filter($this->env, (isset($context["grid_period"]) ? $context["grid_period"] : null), "html", null, true);
        echo "</h5></div>
\t\t<div class=\"ui-block-b\"><a data-role=\"button\" href=\"#page1\" data-icon=\"check\" data-iconpos=\"notext\"></a></div>
\t\t<div class=\"ui-block-c\"><a data-role=\"button\" href=\"#page1\" data-icon=\"check\" data-iconpos=\"notext\"></a></div>
\t\t<div class=\"ui-block-d\"><a data-role=\"button\" href=\"#page1\" data-icon=\"check\" data-iconpos=\"notext\"></a></div>
\t\t<div class=\"ui-block-e\"><a data-role=\"button\" href=\"#page1\" data-icon=\"check\" data-iconpos=\"notext\"></a></div>
\t</div>
</div>";
    }

    public function getTemplateName()
    {
        return "OSULibraryOpenroomBundle:Default:hour-grid.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  111 => 49,  99 => 42,  87 => 35,  75 => 28,  49 => 12,  43 => 9,  39 => 8,  35 => 7,  22 => 2,  19 => 1,  159 => 137,  156 => 133,  153 => 129,  150 => 125,  147 => 121,  144 => 117,  141 => 113,  138 => 109,  135 => 105,  132 => 101,  129 => 97,  126 => 93,  124 => 89,  121 => 88,  118 => 85,  115 => 81,  112 => 77,  109 => 73,  106 => 69,  103 => 65,  100 => 61,  97 => 57,  94 => 53,  91 => 49,  88 => 45,  86 => 41,  82 => 40,  79 => 36,  62 => 20,  59 => 19,  54 => 16,  47 => 13,  44 => 12,  34 => 4,  31 => 6,);
    }
}
