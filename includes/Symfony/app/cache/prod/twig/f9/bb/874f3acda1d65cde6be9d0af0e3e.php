<?php

/* OSULibraryOpenroomBundle:Default:index.html.twig */
class __TwigTemplate_f9bb874f3acda1d65cde6be9d0af0e3e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("OSULibraryOpenroomBundle:Default:template.html.twig");

        $this->blocks = array(
            'header' => array($this, 'block_header'),
            'title' => array($this, 'block_title'),
            'message' => array($this, 'block_message'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "OSULibraryOpenroomBundle:Default:template.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_header($context, array $blocks = array())
    {
        // line 4
        echo "\t<a data-role=\"button\" href=\"#page1\" data-icon=\"home\" data-iconpos=\"left\" class=\"ui-btn-left\">
\t\tHome
\t</a>
\t<a data-role=\"button\" href=\"#page1\" class=\"ui-btn-right\">
\t\tLogin
\t</a>
";
    }

    // line 12
    public function block_title($context, array $blocks = array())
    {
        // line 13
        echo "\t<h3> Study Rooms </h3>
";
    }

    // line 16
    public function block_message($context, array $blocks = array())
    {
        // line 17
        echo "<p>You can reserve up to 28 days in advance.</p>
";
    }

    // line 20
    public function block_body($context, array $blocks = array())
    {
        // line 21
        echo "<ul data-role=\"listview\" data-divider-theme=\"b\" data-inset=\"true\">
\t<li data-role=\"list-divider\" role=\"heading\">
\t\tFloors
\t</li>
\t<li data-theme=\"c\">
    \t<a href=\"floors/1\" data-transition=\"slide\">
        \t1st Floor
\t\t</a>
\t</li>
\t<li data-theme=\"c\">
\t\t<a href=\"floors/2\" data-transition=\"slide\">
\t\t\t2nd Floor
\t\t</a>
\t</li>
\t<li data-theme=\"c\">
\t\t<a href=\"floors/5\" data-transition=\"slide\">
\t\t\t5th Floor
\t\t</a>
\t</li>
\t<li data-theme=\"c\">
\t\t<a href=\"floors/6\" data-transition=\"slide\">
\t\t\t6th Floor
\t\t</a>
\t</li>
</ul>
";
    }

    public function getTemplateName()
    {
        return "OSULibraryOpenroomBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  63 => 21,  60 => 20,  55 => 17,  52 => 16,  47 => 13,  44 => 12,  34 => 4,  31 => 3,);
    }
}
