<?php

/* OSULibraryOpenroomBundle:Default:template.html.twig */
class __TwigTemplate_e1ac326cd20d868988fe1f0b01e149a4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'header' => array($this, 'block_header'),
            'title' => array($this, 'block_title'),
            'message' => array($this, 'block_message'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\">
<head>
\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\" />
    <link rel=\"stylesheet\" href=\"http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.css\" />
\t<script src=\"http://code.jquery.com/jquery-1.8.2.min.js\"></script>
\t<script src=\"http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.js\"></script>
\t
\t<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">
\t<title>Openroom</title>
\t<!-- <link href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("join_files/main.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\"> -->
</head>

<body>

<!-- Home -->
<div id=\"main\" data-role=\"page\" id=\"page1\">
\t<div data-theme=\"d\" data-role=\"header\">
\t\t";
        // line 19
        $this->displayBlock('header', $context, $blocks);
        // line 21
        echo "\t\t
\t\t";
        // line 22
        $this->displayBlock('title', $context, $blocks);
        // line 24
        echo "\t</div>
\t<div id=\"content\" data-role=\"content\">
\t\t";
        // line 26
        $this->displayBlock('message', $context, $blocks);
        // line 28
        echo "\t\t
    \t";
        // line 29
        $this->displayBlock('body', $context, $blocks);
        // line 31
        echo "    </div>
</div>

</body></html>";
    }

    // line 19
    public function block_header($context, array $blocks = array())
    {
        // line 20
        echo "\t\t";
    }

    // line 22
    public function block_title($context, array $blocks = array())
    {
        // line 23
        echo "\t\t";
    }

    // line 26
    public function block_message($context, array $blocks = array())
    {
        // line 27
        echo "\t\t";
    }

    // line 29
    public function block_body($context, array $blocks = array())
    {
        // line 30
        echo "    \t";
    }

    public function getTemplateName()
    {
        return "OSULibraryOpenroomBundle:Default:template.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  95 => 30,  92 => 29,  88 => 27,  85 => 26,  81 => 23,  78 => 22,  74 => 20,  71 => 19,  64 => 31,  62 => 29,  59 => 28,  57 => 26,  53 => 24,  51 => 22,  48 => 21,  46 => 19,  35 => 11,  23 => 1,  63 => 21,  60 => 20,  55 => 17,  52 => 16,  47 => 13,  44 => 12,  34 => 4,  31 => 3,);
    }
}
