		//reset type=date inputs to text
		$( document )
			.on( "mobileinit", function(){
$.mobile.page.prototype.options.degradeInputs.date = true;
		})
			.on( "pageinit", function(){
$( "input[type='date'], input:jqmData(type='date')" ).each(function()
	{
	$(this)
		.replaceWith( $( "<div />" ).datepicker(
			{
			altField			: "#" + $(this).attr( "id" ),
			altFormat			: "dd/mm/yy",
			showOtherMonths		: true,
			selectOtherMonths	: false,
			showWeek			: false,
			changeYear			: false,
			changeMonth			: false,
              beforeShowDay: function(date)
	{
		
	return [true];
	}
	}));
	});

/*$( '.hasDatepicker' ).hide();
$( "input[type='date'], input:jqmData(type='date')" ).focus(function() {$( '.hasDatepicker' ).show('fast');});
$( '.ui-body-c a' ).on('click', function() {$( '.hasDatepicker' ).hide('fast');});*/
});
	