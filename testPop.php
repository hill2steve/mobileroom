<?php 
	
$dbhost = "localhost";
$dbuser = "openroom";
$dbdatabase = "openroom";
$dbpass = "password";
mysql_connect($dbhost, $dbuser, $dbpass) or die('Can\'t connect to the database. Error: ');
mysql_select_db($dbdatabase) or die('Can\'t connect to the database. Error: ');
 
	
	/**
	 * 
	 * @param $today
	 * 
	 * Today should be the last 3 elements of mktime, month, day, year, in an array such that
	 * $today[0] => 'month'
	 * $today[1] => 'day'
	 * $today[2] => 'year'
	 * 
	 * @param $attempts
	 * 
	 * The number of reservations that will be attempted, a suggested amount is unknown as of now.
	 * 
	 * @param $arr 
	 * 
	 * Arr is the array that contains all of the room names.
	 * 
	 */
	$lmresult = mysql_query("select roomid from rooms") or die (mysql_error());
	$i = 0;
	while($lmrecord = mysql_fetch_array($lmresult)){
		$arr[$i] = $lmrecord[0];
		$i++;	
	} 
	// 50  25 %
	// 200 50 %
	// 500 75 %
	//5k   95 %
	//10k very full 99
	//50k very very full >99
	// 100k full 100
	if( isset($_POST['year']) && isset($_POST['month']) && isset($_POST['day']) && isset($_POST['attempts']) ){
		$today[0] = $_POST['month'];
		$today[1] = $_POST['day'];
		$today[2] = $_POST['year'];
		$attempts = $_POST['attempts'];
		
		randomReservations($today, $attempts, $arr);
		
	}
	else{
		echo 
		"<HTML>
		<BODY>
			<h2>SCRIPT FOR GENERATING RESERVATIONS</h2>
			Note: Running this script will remove all reservations for the given day.<br/>
			Use the following form to generate a random number of reservations over the course of a certain day. 
			Although the reservation count is out of our control, 
			you can tell the program how many times you would like 
			to try and reserve rooms. Here are some basic guidelines for determining how many attempts to choose:
			<p/>
			25% full -- 50 attempts <br/>
			50% full -- 200 attempts <br/>
			75% full -- 500 attempts <br/>
			95% full -- 5000 attempts <br/>
			99% full -- 10000 attempts <br/>
			>99% full -- 50000 attempts <br/>
			100% full -- 100000 attempts <br/>
			
			<br/>
			You might notice that the attempts are logarithmic, that's  because the program is randomly picking times and durations. As the rooms become more full, it guesses wrong more often, which in turns makes more attempts.
		
			<form name = 'test' method = 'POST' action = testPop.php>
				Year(yyyy): <input type = 'text' name = 'year'></input><br/>
				Month (mm): <input type = 'text' name = 'month'></input><br/>
				Day(dd): <input type = 'text' name = 'day'></input><br/>
				Attempts: <input type = 'text' name = 'attempts'></input><br/>
				<input type = 'submit' value = 'submit' /><br/>
			</form>
		</BODY>
		</HTML>";
	}

	
	function randomReservations( $today, $attempts, $arr){
		//the othe thing this function should probably do is clear all reservations on that day.
		
	//	echo "Deleting reservations...... <br/>";
	//	mysql_query("TRUNCATE TABLE reservations") or die(mysql_error());
		$reservations = 0;
		$month = $today[0];
		$day = $today[1];
		$year = $today[2];
		$time = mktime(0,0,0,$month,$day,$year);
		
		//just so we know cache is a thing
		// its of the format [room][entry][start/end]
		// [room]['entry'] represents the total number of reservations of that room, to get data on a specific one
		// 		you would grab [room][int entry], it would return the array with start/end
		$cache;
		
		
		//attempts start here
		
		
		for($i = 0; $i < $attempts; $i++){
			//pick a room
			$flag = '';
			$room = $arr[rand(0, sizeof($arr) -1)];	
			//pick a time and duration
			$starthr = rand(0,24) * 3600;
			$startmin = rand(0,6) * 600;
			$starttime = $starthr + $startmin;
		
			$endtime = $starttime + rand(1,36)*600;
			$endtime = $endtime-1; //FOR COLLISIONS SAKE!!!!!!
			
			//check the cache for collision
			
			for($j = 0; $j< $cache[$room]['entries']; $j++){
				if( !($starttime > $cache[$room][$j]['endtime'] || $endtime < $cache[$room][$j]['starttime']) ) {
					//if it starts after the end or ends before the start you're okay :D, otherwise you are not O:
					$flag = 'die';
					echo "Collision... generating next random room..... <br/>";
					break;
				}
			}
			
			if($flag != 'die'){
				$entry = $cache[$room]['entries'];
				$cache[$room]['entries']++;
				$cache[$room][$entry]['starttime'] = $starttime;
				$cache[$room][$entry]['endtime'] = $endtime;
				$start = date("Y-m-d H:i:s",$time + $starttime);
				$end = date("Y-m-d H:i:s", $time + $endtime);
				mysql_query("INSERT INTO reservations(start,end,roomid,username,numberingroup) VALUES('$start', '$end', $room, 'autogen', '1')") or die(mysql_error());
				//sql stuff
				echo "Inserting reservation at: ". $start . " " . $end . " " . $room . "<br/>";
				$reservations++;
			
			}
			
		}
		echo $reservations . " reservations made in " .$attempts . "attempts. <br/>";
		
		
		
	}
?>
